﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using SharedMongoDBModelLibrary;

namespace СourseСatalog.Infrastructure.Repositories.Implementations
{
    public class CourseRepository : ICourseRepository
    {
        private readonly IMongoCollection<Course> _coursesCollection;

        public CourseRepository(
            IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                courceStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(courceStoreDatabaseSettings.Value.DatabaseName);

            _coursesCollection = mongoDatabase.GetCollection<Course>(courceStoreDatabaseSettings.Value.BooksCollectionName);
        }

        public async Task<List<Course>> GetAsync() =>
            await _coursesCollection.Find(_ => true).ToListAsync();
    }
}
