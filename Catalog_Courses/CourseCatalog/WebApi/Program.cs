using Microsoft.OpenApi.Models;
using �ourse�atalog.Background;
using �ourse�atalog.Infrastructure;
using �ourse�atalog.Infrastructure.Repositories.Implementations;
using �ourse�atalog.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();

builder.Configuration.AddJsonFile("WebApi\\appsettings.json", optional: false, reloadOnChange: true);

builder.Services.Configure<CourseStoreDatabaseSettings>(
    builder.Configuration.GetSection("CourseStoreDatabase"));

builder.Services.AddSingleton<ICourseRepository, CourseRepository>();
builder.Services.AddSingleton<IPromotionsService, PromotionService>();
builder.Services.AddSingleton<PromotionServiceCached>();
builder.Services.AddHostedService<PromotionUpdateService>();

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Course catalog API",
        Description = "������� ������. ������ ����� ������ � �� ���������� (�������������� ������������� � ������).",
    });
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        options.RoutePrefix = string.Empty;
    });
}
app.MapControllers();
app.Run("https://localhost:7209");
