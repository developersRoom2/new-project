﻿using СourseСatalog.Domain;
using СourseСatalog.Infrastructure.Repositories.Implementations;
using СourseСatalog.WebApi.Models;

namespace СourseСatalog.Services
{
    public class PromotionService : IPromotionsService
    {
        private readonly ICourseRepository _courseRepository;

        public PromotionService(ICourseRepository courseRepository)
        {
            _courseRepository = courseRepository;
        }

        public async Task<List<Promotion>> GetPromotions()
        {
            var courses = await _courseRepository.GetAsync();
        
            return new List<Promotion>(courses.Select(c => new Promotion()
            {
                Id = c.Id,
                Title = c.Title,
                Description = c.Description,
                Promo = c.Promotion,
                Lessons = new List<Lesson>(c.Lessons.Select(l => 
                new Lesson() { Id = l.Id, Title = l.Title, Description = l.Description}).ToList())
            }).ToList());
        }
    }
}
