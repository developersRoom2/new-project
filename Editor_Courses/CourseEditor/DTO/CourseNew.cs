﻿namespace CourseEditorContract
{
    public class CourseNew
    {
        //public string? Id { get; set; }
        public string Title { get; set; } = null!;
        public string Promotion { get; set; } = null!;
        public string Description { get; set; } = null!;
    }
}