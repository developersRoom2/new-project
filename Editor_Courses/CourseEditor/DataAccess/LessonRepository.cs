﻿using CourseEditor.DataAccess;
using CourseEditor.Model;
using CourseEditor.Pattern;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Driver;
using SharedMongoDBModelLibrary;
using System.Diagnostics.CodeAnalysis;

namespace CourseEditor.DataAccess
{
    public class LessonRepository : ILessonRepository
    {
        private readonly IMongoCollection<Course> _coursesCollection;

        public LessonRepository(
            IOptions<CourseStoreDatabaseSettings> courceStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                courceStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(courceStoreDatabaseSettings.Value.DatabaseName);

            _coursesCollection = mongoDatabase.GetCollection<Course>(courceStoreDatabaseSettings.Value.BooksCollectionName);
        }

        public async Task<DBResponse> NewLesson(string courseId,  string title, string description,  string? homework,  string? links,  List<CourseEditorContract.Question>? test)
        {
            try
            {
                var bldr = new LessonBuilder(title, description);

                if (homework != null && homework.Trim() != "") bldr.AddHomework(homework);
                if (links != null && links.Trim() != "") bldr.AddLinks(links);
                if (test != null && test.Count != 0)
                {
                    bldr.AddTest();
                    var it = new TestIterator(test);
                    while (it.HasNext())
                    {
                        bldr.AddTestQuestion(it.GetNext());
                    }
                }

                var lesson=bldr.Build();

                Course course = _coursesCollection.Find(c => c.Id == courseId).First();

                if (course is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course not found" };
                }               

                course.Lessons.Add(lesson);
                course.DateUpdated = DateTime.Now;

                await _coursesCollection.FindOneAndReplaceAsync(c => c.Id == course.Id, course);
                return new DBResponse { Code = Codes.Ok,Id=lesson.Id.ToString() };
            }
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString() };
            }
        }

        public async Task<DBResponse> EditLesson(string courseId, Lesson lesson)
        {
            try 
            {
                Course course = _coursesCollection.FindAsync(c => c.Id == courseId).Result.First();
                if (course is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course not found" };
                }

                lesson.DateUpdated = DateTime.Now;
                course.DateUpdated = DateTime.Now;

                Lesson oldLesson = course.Lessons.Find(c => c.Id == lesson.Id);

                if (oldLesson is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Lesson not found" };
                }

                var filter = Builders<Course>.Filter.Eq(x => x.Id, courseId)
                    & Builders<Course>.Filter.ElemMatch(x => x.Lessons, Builders<Lesson>.Filter.Eq(x => x.Id, lesson.Id));

                var update = Builders<Course>.Update.Set(x => x.Lessons[-1], lesson);

                await _coursesCollection.UpdateOneAsync(filter, update);

                //oldLesson = lesson;
                //await _coursesCollection.FindOneAndReplaceAsync(c => c.Id == course.Id, course);
                return new DBResponse { Code = Codes.Ok, Id = lesson.Id.ToString() };
            }
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString()    };
            }
        }

        public async Task<DBResponse> DelLesson(string courseId, string id)
        {
            try
            {

                Course course = _coursesCollection.FindAsync(c => c.Id == courseId).Result.First();
                if (course is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course not found" };
                }

                Lesson oldLesson = course.Lessons.Find(c => c.Id == id);
                if (oldLesson is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Lesson not found" };
                }

                oldLesson.DateUpdated = DateTime.Now;
                course.DateUpdated = DateTime.Now;
                oldLesson.IsRemoved = true;

                var filter = Builders<Course>.Filter.Eq(x => x.Id, courseId)
                    & Builders<Course>.Filter.ElemMatch(x => x.Lessons, Builders<Lesson>.Filter.Eq(x => x.Id, oldLesson.Id));

                var update = Builders<Course>.Update.Set(x => x.Lessons[-1], oldLesson);

                await _coursesCollection.UpdateOneAsync(filter, update);

                var record = await _coursesCollection.FindOneAndReplaceAsync(c => c.Id == course.Id, course);
                return new DBResponse { Code = record != null ? Codes.Ok : Codes.Fail, ResultText = $"deleted {id}", Id = id };
            }
             
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText=e.ToString()    };
            }
        }


        public Lesson Get(string courseId, string id)
        {
            try
            {
                var c = _coursesCollection.Find(x => x.Id == courseId).FirstOrDefault();

                if (c is null) return null;
                return c.Lessons.Find(l => l.Id == id);
            }
            catch  { return null; }
            
        }


        public List<Lesson> Get(string courseId)
        {
            try
            {
                var c = _coursesCollection.Find(x => x.Id == courseId).FirstOrDefault();
                if (c is null) return new List<Lesson>();
                return c.Lessons.Where(x => x.IsRemoved == false).ToList();
            }
            catch { return null; }
        }
        

        public async Task<DBResponse> ChangeOrder(string courseId, string id, int order)
        {
            try
            {
                Course course = _coursesCollection.FindAsync(c => c.Id == courseId).Result.First();
                if (course is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Course not found" };
                }

                Lesson oldLesson = course.Lessons.Find(c => c.Id == id);
                if (oldLesson is null)
                {
                    return new DBResponse { Code = Codes.Fail, ResultText = "Lesson not found" };
                }

                if (course.Lessons.Remove(oldLesson))
                {
                    course.Lessons.Insert(order, oldLesson);
                    course.DateUpdated = DateTime.Now;
                }             


                await _coursesCollection.FindOneAndReplaceAsync(c => c.Id == course.Id, course);
                return new DBResponse { Code = Codes.Ok, Id = oldLesson.Id.ToString() };
            }
            catch (Exception e)
            {
                return new DBResponse { Code = Codes.Fail, ResultText = e.ToString() };
            }

        }
    }
}
