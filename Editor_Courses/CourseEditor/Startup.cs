﻿using CourseEditor.DataAccess;
using CourseEditor.Mappings;
using CourseEditor.Samples;
using CourseEditor.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor
{
    public class Startup
    {
        private readonly IConfigurationRoot _config;
        public Startup(IHostEnvironment env)
        {
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json",true)
                .AddEnvironmentVariables();
            _config = builder.Build();
        }

        public void Configure(IApplicationBuilder application,IWebHostEnvironment env)
        {
            application.UseHealthChecks("/health");
            application.UseAuthentication();
            application.UseAuthorization();
            application.UseRouting();
            application.UseEndpoints(e=>e.MapControllers());
            

            
                application.UseSwagger();
                application.UseSwaggerUI();
            
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHealthChecks();

            services.Configure<CourseStoreDatabaseSettings>(_config.GetSection("CourseStoreDatabase"));

            services.AddSingleton<ICourseRepository, CourseRepository>();
            services.AddSingleton<ILessonRepository, LessonRepository>();

            services.AddAutoMapper(typeof(CourseMapping),typeof(LessonMapping));

            services.AddControllers();
            services.AddEndpointsApiExplorer();
            //services.AddSwaggerGen(c=> c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First()));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo
                    {
                        Title = "My API - V1",
                        Version = "v1"
                    }
                 );

                var filePath = Path.Combine(System.AppContext.BaseDirectory, "MyApi.xml");
                c.IncludeXmlComments(filePath);
            });

            //var connectionString = Environment.GetEnvironmentVariable("MongoConnection") ?? _config.GetValue<string>("MongoConnection");
            //services.AddSingleton(new MongoClient(connectionString));

        }
    }
}
