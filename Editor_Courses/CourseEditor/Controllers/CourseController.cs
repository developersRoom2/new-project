﻿using CourseEditor.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
//using SharedMongoDBModelLibrary;
using CourseEditorContract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using AutoMapper;
using CourseEditor.DataAccess;

namespace CourseEditor.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CourseController : ControllerBase
    {
   
        private readonly ILogger<CourseController> _logger;
        private readonly ICourseRepository _repo;
        private readonly IMapper _mapper;


        public CourseController(ILogger<CourseController> logger,ICourseRepository repo,IMapper mapper)
        {
            _logger = logger;
            _repo = repo;
            _mapper = mapper;
        }

        /// <summary>
        /// Создать новый курс
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<DBResponse> Add([FromHeader] string title, [FromHeader] string promotion, [FromHeader] string description )
        {
            return await  _repo.NewCourse( title,  promotion,  description);
        }

        [HttpDelete]
        public async Task<DBResponse> Delete([FromHeader] string id)
        {
            return await _repo.DelCourse( id);
        }

        /// <summary>
        /// Редактировать курс
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<DBResponse> Update([FromBody] CourseEditorContract.CourseHdr course)
        {
            return await _repo.EditCourse(_mapper.Map<SharedMongoDBModelLibrary.Course>(course));
        }

        /// <summary>
        /// Список курсов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task< IEnumerable<CourseEditorContract.CourseHdr>> GetAll()
        {
            return _mapper.Map <IEnumerable <CourseEditorContract.CourseHdr >>(await _repo.GetAsync());
        }
        
        /// <summary>
        /// Посмотреть курс
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<CourseEditorContract.Course> Get(string id)
        {
            return _mapper.Map<CourseEditorContract.Course>(await _repo.GetAsync( id));
        }

    
        [HttpGet]
        public async Task<DBResponse> MakeSamples( )
        {
            return await  _repo.MakeSamples();
        }

        [HttpGet]
        public void DropDatabase()
        {
            _repo.DropDatabase();
        }


    }
}
