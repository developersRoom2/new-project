﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourseEditor.Model
{
    public class DBResponse
    {
        public string? Id { get; set; }
        public string? ResultText { get; set; }
        public string? Code { get; set; }

    }
}
