﻿using CourseEditor.Samples;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;

namespace CourseEditor
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            try
            {
                Log.Information("Запуск");
                Log.Logger  = new LoggerConfiguration()
                    .Enrich.FromLogContext()
                    .Enrich.WithProperty("ApplicationName", "CourseEditor")
                    .WriteTo.Console(outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                    .CreateLogger();

                IHost host = Host.CreateDefaultBuilder(args)
                    .UseSerilog()
                    .ConfigureWebHostDefaults(wb => { wb.UseStartup<Startup>().UseUrls(urls: "http://localhost:10000"); })
                    .Build();

                await host.RunAsync();



            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "аварийное завершение");
            }
            finally
            {
                Log.Information("Завершено");
                Log.CloseAndFlush();
            }
        }
    }

}