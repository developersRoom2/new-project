﻿using AutorizationMcsContract;
using ShluzAPIcontroller.Models;

namespace ShluzAPIcontroller.Services.Abstraction
{
    public interface IUserService
    {
        public User Get(UserLogin userLogin);
        public int Set(UserAutorizationModel user);
    }
}
