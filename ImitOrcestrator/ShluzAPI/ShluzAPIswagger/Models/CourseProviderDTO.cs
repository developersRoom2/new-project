﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using SharedMongoDBModelLibrary;

namespace Learn_Platform_shluz.Models
{
    public class CourseProviderDTO
    {
        public string? Id { get; set; }
        public string Title { get; set; } = null!;
        //public string Promotion { get; set; } = null!;
        public string Description { get; set; } = null!;
        //public DateTime DateCreated { get; set; }
        //public DateTime DateUpdated { get; set; }
        //public bool IsRemoved { get; set; }
        public List<LessonDTO> Lessons { get; set; } = null!;

        public class LessonDTO
        {
            public string? Id { get; set; }
            public string Title { get; set; } = null!;
            public string Description { get; set; } = null!;
            public string Homework { get; set; } = null!;
            //public DateTime DateCreated { get; set; }
            //public DateTime DateUpdated { get; set; }
            //public bool IsRemoved { get; set; }
            public List<Question> Test { get; set; } = null!;
            public List<string> Links { get; set; } = null!;
        }

        public class Question
        {
            public string Text { get; set; } = null!;
            public List<Response> Responses { get; set; } = null!;
        }

        public class Response
        {
            public string Text { get; set; } = null!;
            //public bool IsTrue { get; set; }
        }
    }
}
