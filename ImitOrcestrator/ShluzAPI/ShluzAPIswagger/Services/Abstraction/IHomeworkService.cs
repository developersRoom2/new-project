﻿using Learn_Platform_shluz.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface IHomeworkService
    {
        int Submit(long StudentId, string CourseId, string LessonId, string Text);

        List<HomeworkDTO> GetAllHomework(string courseId);

        List<MessageDTO> AcceptHomework(int homeworkId, int mark);

        List<MessageDTO> GetAllChat(int homeworkId, string role);

        int HomeworkMessage(string text, int userId, int homeworkId, string role);
    }
}
