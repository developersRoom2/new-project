﻿using ShluzAPIswagger.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface ICatalogService
    {
        public List<Promotion> Get();
    }
}
