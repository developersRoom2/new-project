﻿using Learn_Platform_shluz.Models;
using ShluzAPIswagger.Models;

namespace ShluzAPIswagger.Services.Abstraction
{
    public interface ICourseEditorService
    {
        public int DeleteCourse(string courseId);

        public int DeleteLesson(string courseId, string lessonId);

        public DBResponse AddCourse(string title, string promotion, string description);

        public int AddLesson(Models.LessonDTO lesson);

        public int UpdateCourse(CourseDTO course);

        public int UpdateLesson(string courseId, LessonAddDTO lesson);
    }
}
