﻿using ShluzAPIswagger.Services.Abstraction;
using System.Text.Json;
using System.Text;
using Microsoft.Net.Http.Headers;
using System.Net.Http;
using ShluzAPIswagger.Models;
using SharedMongoDBModelLibrary;
using Learn_Platform_shluz.Models;
using static Learn_Platform_shluz.Models.CourseProviderDTO;

namespace ShluzAPIswagger.Services.Implementation
{
    public class CourseEditorService : ICourseEditorService
    {
        public DBResponse AddCourse(string title, string promotion, string description)
        {
            try
            {
                return AddCourseAsync(title, promotion, description).Result;
            }
            catch (Exception ex)
            { 
                Console.WriteLine(ex);
                return null;
            }
        }

        public int AddLesson(Models.LessonDTO lesson)
        {
           
            try
            {
                return AddLessonAsync(lesson).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            }
        }

        /// <summary>
        /// DELETE COURSE 
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        public int DeleteCourse(string courseId)
        {
            return DeleteCourseAsync(courseId).Result;
        }

        public int DeleteLesson(string courseId, string lessonId)
        {
            return DeleteLessonAsync(courseId, lessonId).Result;
        }

        public int UpdateCourse(CourseDTO course)
        {
            return UpdateCourseAsync(course).Result;
        }

        public int UpdateLesson(string courseId, LessonAddDTO lesson)
        {
            return UpdateLessonAsync(courseId, lesson).Result;
        }

        async Task<int> DeleteCourseAsync(string courseId)
        {
            string url = "http://localhost:10000/api/Course/Delete"; // 

            int result = 0;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("id", courseId);

                var response = await client.DeleteAsync(url);

                result = 1;
            }

            return result;
        }

        async Task<int> DeleteLessonAsync(string courseId, string lessonId)
        {
            string url = "http://localhost:10000/api/Lesson/Delete"; // 

            int result = 0;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("courseId", courseId);
                client.DefaultRequestHeaders.Add("id", lessonId);

                var response = await client.DeleteAsync(url);

                result = 1;
            }

            return result;
        }

        async Task<DBResponse> AddCourseAsync(string title, string promotion, string description)
        {
            string url = "http://localhost:10000/api/Course/Add"; // 

            DBResponse result = null;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("title", title);
                client.DefaultRequestHeaders.Add("promotion", promotion);
                client.DefaultRequestHeaders.Add("description", description);

                var response = await client.PostAsync(url, null);

                result = await response.Content.ReadFromJsonAsync<DBResponse>();
            }

            return result;
        }

        async Task<int> AddLessonAsync(Models.LessonDTO lesson)
        {
            string url = "http://localhost:10000/api/Lesson/Add"; // 

            //var json = JsonSerializer.Serialize(lesson.Test);
            string json = null;
            HttpContent cont = null;
            int result = 0;

            if (lesson.Test != null)
            {
                json = JsonSerializer.Serialize(lesson.Test);
                cont = new StringContent(json, Encoding.UTF8, "application/json");
            }


            using (var client = new HttpClient())
            {

                try
                {

                    //fill in header
                    client.DefaultRequestHeaders.Add("courseId", lesson.courseId);
                    client.DefaultRequestHeaders.Add("Title", lesson.Title);
                    client.DefaultRequestHeaders.Add("Description", lesson.Description);
                    if (lesson.Homework != null)
                        client.DefaultRequestHeaders.Add("Homework", lesson.Homework);
                    if (lesson.Links != null)
                        client.DefaultRequestHeaders.Add("Links", lesson.Homework);



                    var response = await client.PostAsync(url, cont);

                    result = 1;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to http://localhost:10000/api/Lesson/Add!");

                    if (cont != null)
                        cont.Dispose();

                    return -1;
                }
            }

            if (cont != null)
                cont.Dispose();

            return result;
        }

        async Task<int> UpdateCourseAsync(CourseDTO course)
        {
            string url = "http://localhost:10000/api/Course/Update"; // 

            var json = JsonSerializer.Serialize(course);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PutAsync(url, cont);
                        //result = Int32.Parse(await response.Content.ReadAsStringAsync());
                        result = 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to http://localhost:10000/api/Course/Update!");
                    return -1;
                }
            }

            return result;
        }

        async Task<int> UpdateLessonAsync(string courseId, LessonAddDTO lesson)
        {
            string url = "http://localhost:10000/api/Lesson/Update"; // 

            var json = JsonSerializer.Serialize(lesson);
            int result = 0;

            using (var client = new HttpClient())
            {
                //fill in header
                client.DefaultRequestHeaders.Add("courseId", courseId);

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PutAsync(url, cont);
                        
                        result = 1;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request to http://localhost:10000/api/Lesson/Update!");
                    return -1;
                }
            }

            return result;
        }
    }
}
