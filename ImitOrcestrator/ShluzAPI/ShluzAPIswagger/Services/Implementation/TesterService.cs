﻿using SharedMongoDBModelLibrary;
using ShluzAPIswagger.Models;
using ShluzAPIswagger.Services.Abstraction;
using System.Text.Json;
using System.Text;
using System.Collections.Generic;

namespace ShluzAPIswagger.Services.Implementation
{
    public class TesterService : ITesterService
    {
        public List<Question> GetTest(int userId, string courseId, string lessonId)
        {
            try
            {
                return GetQuestions(userId, courseId, lessonId).Result;
            }
            catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);
                return null;
            }
            
        }

        public int SendAnswers(int userId, string courseId, string lessonId, List<Question> test)
        {
            try
            {
                return PostTestAnswersAsync(userId, courseId, lessonId, test).Result;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return -1;
            }
        }

        async Task<List<Question>> GetQuestions(int userId, string courseId, string lessonId)
        {
            string url = "https://localhost:7052/api/Test/" + userId.ToString() + "/" + courseId + "/" + lessonId; // 

            List<Question> questions = null;

            using (var client = new HttpClient())
            {
                //  
                var task = await client.GetFromJsonAsync(url, typeof(List<Question>));
                questions = (List<Question>)task;
            }

            return questions;
        }

        async Task<int> PostTestAnswersAsync(int userId, string courseId, string lessonId, List<Question> test)
        {
            string url = "https://localhost:7052/api/Test/answers"; // 

            TestDTO testAnswers = new TestDTO() { UserId = userId, CourseId = courseId, LessonId = lessonId, answers = test};

            var json = JsonSerializer.Serialize(testAnswers);
            int result = 0;

            using (var client = new HttpClient())
            {

                try
                {
                    using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                    {
                        var response = await client.PostAsync(url, cont);
                        result = int.Parse(await response.Content.ReadAsStringAsync());
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Bad request!");
                }
            }

            return result;
        }
    }
}
