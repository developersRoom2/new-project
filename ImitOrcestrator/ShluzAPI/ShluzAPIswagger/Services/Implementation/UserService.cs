﻿using AutorizationMcsContract;
using ShluzAPIswagger.Models;
using System.Text;
using System.Text.Json;
using ShluzAPIswagger.Services.Abstraction;

namespace ShluzAPIswagger.Services.Implementation
{
    public class UserService : IUserService
    {
        public User Get(UserLogin userLogin)
        {
            User user = new User();
            string uRole = null;

            try
            {
                //Request to AuthrizationMcs
                int resUserId = GetUserAsync(userLogin.Username, userLogin.Password).Result;
                //check existing of User
                //if user exist request his role
                if (resUserId != 0)
                {
                    uRole = GetUserRoleAsync(resUserId).Result;
                }
                else
                    return null;

                //...
                // Map from UserAutorizationModel and UserRole to User
                user.Id = resUserId;
                user.Email = userLogin.Username;
                user.FirstName = "Empty";
                user.LastName = "Empty";
                user.Role = uRole;
            }
            catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);

                return null;
            }

            return user;
        }

        public int GiveUserRole(int userId, string role)
        {
            int roleId = 0;
            // Kostil :))
            if (role == "Teacher")
                roleId = 2;
            else if (role == "Administrator")
                roleId = 3;
            else 
                roleId = 1;


            return GiveUserRoleAsync(new UserRoleAutorizationModel() { User_Id = userId, Role_Id = roleId, CreateDate = DateTime.Now, UpDate = DateTime.Now, Deleted = false }).Result;
        }

        public int Set(UserAutorizationModel user)
        {
            try
            {
                int userId = SetUserAsync(user).Result;

                if (userId != 0)
                {
                    // Add role "Student" for new user
                    UserRoleAutorizationModel uRole = new UserRoleAutorizationModel
                    {
                        User_Id = userId,
                        Role_Id = 1,
                        UpDate = new DateTime(2022, 3, 1).ToUniversalTime(),
                        CreateDate = new DateTime(2022, 3, 1).ToUniversalTime(),
                        Deleted = false
                    };

                    var uRoleId = SetUserRoleAsync(uRole).Result;

                    if (uRoleId != 0)
                        return userId;
                }
            } catch (Exception ex)
            { 
                Console.WriteLine(ex.Message);
                return -1;
            }

            return 0;
        }

        // Request to AuthorizationMcs
        async Task<int> GetUserAsync(string email, string password)
        {
            string url = "https://localhost:7029/api/User/" + email + "/" + password; //
            int userId = 0;

            using (var client = new HttpClient())
            {
                // Take Questions array from request 
                var task = await client.GetFromJsonAsync(url, typeof(int));
                userId = (int)task;
            }

            return userId;
        }

        // Request to AuthorizationMcs
        async Task<string> GetUserRoleAsync(int userId)
        {
            string url = "https://localhost:7029/api/UserRole/" + userId.ToString(); //

            List<string> roles = null;

            using (var client = new HttpClient())
            {
                // Take Questions array from request 
                var task = await client.GetFromJsonAsync(url, typeof(List<string>));
                roles = (List<string>)task;
            }

            return roles.FirstOrDefault();
        }

        async Task<int> SetUserAsync(UserAutorizationModel user)
        {
            string url = "https://localhost:7029/api/User"; //

            string json = JsonSerializer.Serialize(user);

            using (var client = new HttpClient())
            {
                using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(url, cont);
                    var userId = await response.Content.ReadAsStringAsync();

                    return int.Parse(userId);
                }
            }

            return 0;
        }

        async Task<int> SetUserRoleAsync(UserRoleAutorizationModel userRole)
        {
            string url = "https://localhost:7029/api/UserRole"; //

            string json = JsonSerializer.Serialize(userRole);

            using (var client = new HttpClient())
            {
                using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(url, cont);
                    var userRoleId = await response.Content.ReadAsStringAsync();

                    return int.Parse(userRoleId);
                }
            }

            return 0;
        }

        async Task<int> GiveUserRoleAsync(UserRoleAutorizationModel userRole)
        {
            string url = "https://localhost:7029/api/UserRole"; //

            string json = JsonSerializer.Serialize(userRole);

            using (var client = new HttpClient())
            {
                using (HttpContent cont = new StringContent(json, Encoding.UTF8, "application/json"))
                {
                    var response = await client.PostAsync(url, cont);
                    var userRoleId = await response.Content.ReadAsStringAsync();

                    return int.Parse(userRoleId);
                }
            }

            return 0;
        }
    }
}
