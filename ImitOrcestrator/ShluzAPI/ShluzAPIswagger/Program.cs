using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using ShluzAPIswagger.Models;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using AutorizationMcsContract;
using PostgreSQLModelsLibrary;
using SharedMongoDBModelLibrary;
using ShluzAPIswagger.Services.Abstraction;
using ShluzAPIswagger.Services.Implementation;
using static System.Net.Mime.MediaTypeNames;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;
using System;

namespace ShluzAPIswagger
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Description = "Bearer Authentication with JWT Token",
                    Type = SecuritySchemeType.Http
                });
                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Id = "Bearer",
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory,
                    $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
            });

            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateActor = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = builder.Configuration["Jwt:Issuer"],
                    ValidAudience = builder.Configuration["Jwt:Audience"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"]))
                };
            });
            builder.Services.AddAuthorization();

            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddSingleton<IHomeworkService, HomeworkService>();
            builder.Services.AddSingleton<ITesterService, TesterService>();
            builder.Services.AddSingleton<ICourseProviderService, CourseProviderService>();
            builder.Services.AddSingleton<IAdministratorService, AdministratorService>();
            builder.Services.AddSingleton<ICatalogService, CatalogService>();
            builder.Services.AddSingleton<IUserService, UserService>();
            builder.Services.AddSingleton<ICourseEditorService, CourseEditorService>();

            var app = builder.Build();

            app.UseSwagger();
            app.UseAuthorization();
            app.UseAuthentication();

            app.MapGet("/", () => "----->>>> LEARN PLATFORM !");

            // ������ ������� ������ (��� ����)
            //app.MapGet("/get Courses Catalog (for any visitor)",
            app.MapGet("�����-������� ������ (��� ����)",
                (ICatalogService service) => ListPromo(service));
                //.Produces<List<Promotion>>(statusCode: 200, contentType: "application/json");

            app.MapPost("/����",
                (string email, string password, IUserService service) => Login(email, password, service));

            app.MapPost("/�����������",
                (string FirstName, string LastName, string Email, string Password, IUserService service) => Registration(FirstName, LastName, Email, Password, service));


            // ������ ������ ������ ��������� �������� ����������� �����
            app.MapGet("/����������� ����� (�������/�������)",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (IAdministratorService service, HttpContext context) => GetAccesses(service, context));
            //.Produces<List<Access>>(statusCode: 200, contentType: "application/json");

            app.MapGet("/�������� �� ����� (�������)",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Administrator")]
            (IAdministratorService service, HttpContext context, string courseId) => GetProgresses(service, context, courseId));
            //.Produces<List<Progress>>(statusCode: 200, contentType: "application/json");

            // ������ ��� �����, ��������� � ��
            //app.MapGet("/viewCourses",
            //    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            //    (ICourseProviderService service) => ListCourses(service))
            //    .Produces<int>(statusCode: 200, contentType: "application/json");

            // ������ ��� ���������� �� ����������� �����
            app.MapGet("/���������� ���������� �����",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ICourseProviderService service, string courseId) => GetCourse(service, courseId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ����������� ���� (������� � ���������� �������) ��� ����������� ����� � �����
            app.MapGet("/��������� ���� ��� �����, �� �����",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ITesterService service, HttpContext context, string courseId, string lessonId) => GetTest(service, context, courseId, lessonId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ���������� ����������� ���� �� ����������� ����� � ����� (���������� ����� ��� �������)
            app.MapPost("/��������� ����������� ����",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ITesterService service, HttpContext context, string courseId, string lessonId, List<Question> test) => SendAnswers(service, context, courseId, lessonId, test));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ���������� ������ �� �������� ������� (����� ��)
            app.MapPost("/����� �� (�������)",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Administrator")]
            (IHomeworkService service, HttpContext context, string courseId, string lessonId, string text) => SubmitHmwrk(service, context, courseId, lessonId, text));
            //.Produces<int>(statusCode: 200, contentType: "application/json");          


            // ���������� ������ �������� ��
            app.MapGet("/���������� ������ �������� �� (�������)",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Teacher, Administrator")]
            (IHomeworkService service, string courseId) => GetAllHmwrk(service, courseId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ���������� ��� ��
            app.MapGet("/���������� ��������� ��",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (IHomeworkService service, HttpContext context, int homeworkId) => GetAllChat(service, homeworkId, context));

            // ������� ��������� � ��������� �� 
            app.MapPut("/������� ��������� � ��������� ��",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (IHomeworkService service, string text, HttpContext context, int homeworkId) => HmwrkMessage(service, text, context, homeworkId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ������� �������� �������, ��������� ������ 
            app.MapPost("/������� ��, ��������� ������ (�������)",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Teacher, Administrator")]
            (IHomeworkService service, int homeworkId, int mark) => AcceptHmwrk(service, homeworkId, mark));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ����������� ������ � ����������� ����� ��� �������� (�������) ����� 
            app.MapPost("/��������� ������ � �����",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (IAdministratorService service, HttpContext context, string courseId, string lessonId) => RequestAccess(service, context, courseId, lessonId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ������ ������ �������� �� ������ � ��������� ������, ���������� ������
            app.MapGet("/���������� ������ �������� �� ������ � ������ (�����)",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (IAdministratorService service) => AccessRequests(service));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ������ ������ � ����� �����, �� ������ �������
            app.MapPost("/������ ������ � �����",
            [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (IAdministratorService service, HttpContext context, int userId, string courseId, string lessonId) => GiveAccess(service, context, userId, courseId, lessonId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // �������� ����� ����
            app.MapPost("/�������� ����� ���� (�������/�����)",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ICourseEditorService service, string title, string promotion, string description) => AddCourse(service, title, promotion, description));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // �������� ����� ����
            app.MapPost("/�������� ����� ���� (�������/�����)",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ICourseEditorService service,
            string courseId,
            string Title,
            string Description,
            [AllowNull] string? Homework,
            [AllowNull] string? Links,
            [FromBody, AllowNull] List<Question>? Test) => AddLesson(service, courseId, Title, Description, Homework, Links, Test));
            //.Produces<int>(statusCode: 200, contentType: "application/json");


            // �������� ����
            app.MapPut("/�������� ���� (�������/�����)",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ICourseEditorService service, string id, string title, string description, string promotion) => UpdateCourse(service, id, title, description, promotion));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // �������� ���� 
            app.MapPut("/�������� ���� (�������/�����)",
              [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Student, Teacher, Administrator")]
            (ICourseEditorService service, string courseId, LessonAddDTO lesson) => UpdateLesson(service, courseId, lesson));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            /**********Admin functional***************/

            // ������� ����
            app.MapDelete("/������� ���� (�����)",
                //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
                (ICourseEditorService service, string courseId) => DelCourse(service, courseId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

            // ������� ����
            app.MapDelete("/������� ���� (�����)",
                [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Administrator")]
            (ICourseEditorService service, string courseId, string lessonId) => DelLesson(service, courseId, lessonId));
            //.Produces<int>(statusCode: 200, contentType: "application/json");

         

            // ���������� ���� ������������������ ������ (+ �� ����) .....

            // ������ ���� ����������� �����....

            //app.MapGet("/list", );
            //app.MapPost("/update", );
            //app.MapPost("/delete", );

            IResult Login(string email, string password, IUserService service)
            {
                UserLogin user = new UserLogin() { Username = email, Password = password };

                if (!string.IsNullOrEmpty(user.Username) &&
                    !string.IsNullOrEmpty(user.Password))
                {
                    var loggedInUser = service.Get(user);
                    if (loggedInUser is null) return Results.NotFound("User not found");

                    var claims = new[]
                    {
                        new Claim(ClaimTypes.SerialNumber, loggedInUser.Id.ToString()), // save User Id
                        new Claim(ClaimTypes.Email, loggedInUser.Email),
                        new Claim(ClaimTypes.GivenName, loggedInUser.FirstName),
                        new Claim(ClaimTypes.Surname, loggedInUser.LastName),
                        new Claim(ClaimTypes.Role, loggedInUser.Role)
                    };

                    var token = new JwtSecurityToken
                    (
                        issuer: builder.Configuration["Jwt:Issuer"],
                        audience: builder.Configuration["Jwt:Audience"],
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(60),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(
                            new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Key"])),
                            SecurityAlgorithms.HmacSha256)
                    );

                    var tokenString = new JwtSecurityTokenHandler().WriteToken(token);

                    return Results.Ok(tokenString);
                }

                return Results.BadRequest("Invalid user credentials");
            }

            IResult Registration(string firstName, string lastName, string email, string password, IUserService service)
            {
                var registerUser = service.Set(new UserAutorizationModel() { FirstName = firstName, 
                    LastName = lastName, 
                    Email = email, 
                    Password = password, 
                    CreateDate = DateTime.Now, 
                    UpDate = DateTime.Now, 
                    Deleted = false });

                if(registerUser > 0)
                    return Results.Ok("����� ������������ ������� �������� id = " + registerUser.ToString());
                else 
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult ListPromo(ICatalogService service)
            {
                var promotions = service.Get();

                if (promotions != null)
                    return Results.Ok(promotions);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult GetAccesses(IAdministratorService service, HttpContext context)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var accesses = service.Get(Int32.Parse(_contextUserId));

                if (accesses != null)
                    return Results.Ok(accesses);
                else
                    return Results.Ok("���-�� ����� �� ���...");              
            }

            IResult GetProgresses(IAdministratorService service, HttpContext context, string courseId)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var progresses = service.GetProgress(Int32.Parse(_contextUserId), courseId);

                
                if (progresses != null)
                    return Results.Ok(progresses);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult RequestAccess(IAdministratorService service, HttpContext context, string courseId, string lessonId)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var requestAcc = service.RequestAccess(Int32.Parse(_contextUserId), courseId, lessonId);

                if(requestAcc == 1)
                    return Results.Ok("������ �� ������ ������� ��������� !");
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult GiveAccess(IAdministratorService service, HttpContext context, int userId, string courseId, string lessonId)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var giveAcc = service.GiveAccess(userId, courseId, lessonId);

                if (giveAcc == 1)
                    return Results.Ok("���������� �� ������ ������� ���������� !");
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult ListCourses(ICourseProviderService service)
            {
                var courses = service.GetAll();


                if (courses != null)
                    return Results.Ok(courses);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult GetCourse(ICourseProviderService service, string courseId)
            {
                var courses = service.Get(courseId);

                if (courses != null)
                    return Results.Ok(courses);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult GetTest(ITesterService service, HttpContext context, string courseId, string lessonId)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var test = service.GetTest(Int32.Parse(_contextUserId), courseId, lessonId);

                if (test != null)
                    return Results.Ok(test);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult SubmitHmwrk(IHomeworkService service, HttpContext context, string courseId, string lessonId, string text)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var submit = service.Submit(Int32.Parse(_contextUserId), courseId, lessonId, text);

                if(submit != 0)
                    return Results.Ok("�������� ������� ������� ����� �� ��������, ������������� = " + submit.ToString());
                else
                    return Results.Ok("���-�� ����� �� ���....");
            }

            IResult SendAnswers(ITesterService service, HttpContext context, string courseId, string lessonId, List<Question> test)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;

                var mark = service.SendAnswers(Int32.Parse(_contextUserId), courseId, lessonId, test); 
                
                string reply = string.Empty;

                if (mark > 50)
                    reply = "���� ������� �������, ������� ���������� ������� = " + mark.ToString();
                else
                    reply = "���� �� �������, ������� ���������� ������� = " + mark.ToString();

                return Results.Ok(reply);
            }

            IResult AccessRequests(IAdministratorService service)
            {
                var requests = service.GetAccessRequests(); 

                if (requests != null)
                    return Results.Ok(requests);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult DelCourse(ICourseEditorService service, string courseId)
            { 
                var result = service.DeleteCourse(courseId);


                if (result > 0)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult DelLesson(ICourseEditorService service, string courseId, string lessonId)
            {
                var result = service.DeleteLesson(courseId, lessonId);

                
                if (result > 0)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult AddCourse(ICourseEditorService service, string title, string promotion, string description)
            {
                var result = service.AddCourse(title, promotion, description);

                
                if (result != null)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult AddLesson(ICourseEditorService service, string courseId, string title, string description, string? homework, string? links, List<Question>? test)
            {
                var lesson = new LessonDTO() { courseId = courseId, Title = title, Description = description, Homework = homework, Links = links, Test = test};

                var result = service.AddLesson(lesson);

                if (result > 0)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");   
            }


            IResult UpdateCourse(ICourseEditorService service, string id, string title, string description, string promotion)
            {
                CourseDTO course = new CourseDTO() { Id = id, Title = title, Description = description, Promotion = promotion };

                var result = service.UpdateCourse(course);

                return Results.Ok(result);
            }

            IResult UpdateLesson(ICourseEditorService service, string courseId, LessonAddDTO lesson)
            {
                var result = service.UpdateLesson(courseId, lesson);
                return Results.Ok(result);
            }

            IResult GetAllHmwrk(IHomeworkService service, string courseId)
            {
                var result = service.GetAllHomework(courseId);

                if (result != null)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult GetAllChat(IHomeworkService service, int homeworkId, HttpContext context)
            {
                var _contextUserRole = context.User.Claims.ElementAt(4).Value;
                var result = service.GetAllChat(homeworkId, _contextUserRole);

                
                if (result != null)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            IResult HmwrkMessage(IHomeworkService service, string text, HttpContext context, int homeworkId)
            {
                var _contextUserId = context.User.Claims.ElementAt(0).Value;
                var _contextUserRole = context.User.Claims.ElementAt(4).Value;

                var result = service.HomeworkMessage(text, Int32.Parse(_contextUserId), homeworkId, _contextUserRole);
                
                if(result != 0)
                    return Results.Ok("��������� ������� ����������..");
                else
                    return Results.Ok("���-�� ����� �� ���..");
            }

            IResult AcceptHmwrk(IHomeworkService service, int homeworkId, int mark)
            {
                var result = service.AcceptHomework(homeworkId, mark);

                if (result != null)
                    return Results.Ok(result);
                else
                    return Results.Ok("���-�� ����� �� ���...");
            }

            app.UseSwaggerUI();

            app.Run("https://localhost:7168");
        }
    }
}