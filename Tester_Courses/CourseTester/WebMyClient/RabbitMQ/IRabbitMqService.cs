﻿namespace WebAPIClient.RabbitMQ
{
    public interface IRabbitMqService
    {
        void SendMessage(object obj, string severity);
        void SendMessage(string message, string severity);
    }
}
