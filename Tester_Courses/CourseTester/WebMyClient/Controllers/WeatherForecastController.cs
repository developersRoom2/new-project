using Microsoft.AspNetCore.Mvc;
using WebAPIClient.RabbitMQ;
using PostgreSQLModelsLibrary;

namespace WebMyClient.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IRabbitMqService _mqService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IRabbitMqService mqService)
        {
            _logger = logger;
            _mqService = mqService;
        }

        [HttpGet(Name = "GetWeatherForecast")]
        public IEnumerable<WeatherForecast> Get(string severitySend)
        {
            // Send Message by RabbitMq
            Progress pMess = new Progress();
            pMess.UserId = 1;
            pMess.LessonId = "3";
            pMess.CourseId = "2";
            pMess.DoneHomework = false;
            pMess.IsDone = true;

            _mqService.SendMessage(pMess, severitySend);

            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .ToArray();
        }
    }
}