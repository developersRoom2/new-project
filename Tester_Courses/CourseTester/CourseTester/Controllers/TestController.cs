﻿using Microsoft.AspNetCore.Mvc;
using System.Text;
using System;
using Services.Abstractions;
using Microsoft.AspNetCore.Authorization;
using System.Data;
using TesterContract;
using SharedMongoDBModelLibrary;
using CourseTester.Models;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CourseTester.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        private readonly IRabbitMqService _mqService;

        public TestController(IRabbitMqService mqService)
        {
            _mqService = mqService;
        }

        // GET: api/<ValuesController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        /// <summary>
        /// Оркестратор запрашивает набор вопросов для тестирования юзера
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="courseId"></param>
        /// <param name="lessonId"></param>
        /// <returns></returns>
        // GET api/<ValuesController>/5
        [HttpGet("{userId}/{courseId}/{lessonId}")]
        //[Authorize(Roles = "student")]
        public async Task<IActionResult> Get(int userId, string courseId, string lessonId)
        {
            // Request test Questions array from CourseProvider // Провайдер курса должен прислать в массиве Questions и правильные ответы, чтобы можно было сравнить
            List<Question> qresult = await GetQuestionsAsync(userId, courseId, lessonId);

            // Надо ли обнулить правильные ответы перед передачей их оркестратору ?????? <<-----
            foreach (var q in qresult)
            {
                foreach (var r in q.Responses)
                    r.IsTrue = false;
            }

            return Ok(qresult);
        }

        /// <summary>
        /// Receive answers of user test, check result and send it to Administrator db
        /// </summary>
        /// <param name="value"></param>
        /// 
        // POST api/<ValuesController>
        [HttpPost("answers")]
        public async Task<IActionResult> Post([FromBody] TestDTO test)
        {
            // Request test Questions array from CourseProvider // Провайдер курса должен прислать в массиве Questions и правильные ответы, чтобы можно было сравнить
            List<Question> qright = await GetQuestionsAsync(test.UserId, test.CourseId, test.LessonId);

            // Check result of user test
            int validPercent = 0;
            if (test.answers != null && qright != null)
                validPercent = CheckAnswers(qright, test.answers);

            //validPercent = 100; // Kostil !!!

            // Send by RabbitMQ result of user test to Administrator
            var msgRes = new TestResult() { UserId = test.UserId, CourseId = test.CourseId, LessonId = test.LessonId, PercentCorrect = validPercent };

            

            await SendAdminResultAsync(msgRes);

            // Можно отправить в ответ оркестратору результат тестирования с ошибками и правильными ответами ???

            return Ok(validPercent);
        }

        // PUT api/<ValuesController>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{

        //}

        // DELETE api/<ValuesController>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}

        // Request to CourseProvider
        async Task<List<Question>> GetQuestionsAsync(int userId, string courseId, string lessonId)
        {
            string url = "https://localhost:7078/api/Lesson/"; // ??? какой url тут должен быть, как называется коньроллер в Провайдере Курсов??
            Lesson lesson;

            using (var client = new HttpClient())
            {                               
                // Take Questions array from request 
                var task = await client.GetFromJsonAsync(url + courseId + "/" + lessonId, typeof(Lesson));
                lesson = (Lesson)task;              
            }

            return lesson.Test;
        }

        /// <summary>
        /// Сравнивает ответы юзера с правильными ответами и вычисляет процент правильных ответов, котрый отсылаетв Администратор
        /// </summary>
        /// <param name="qw"></param>
        int CheckAnswers(List<Question> rightq, List<Question> userAnsw)
        {
            int procentPlus = 0;
            float allCnt = 0;

            for (int i = 0; i < userAnsw.Count; ++i)
            {
                for (int j = 0; j < userAnsw[i].Responses.Count; ++j)
                {
                    if (userAnsw[i].Responses[j].IsTrue == rightq[i].Responses[j].IsTrue)
                        procentPlus += 1;
                }
                allCnt += userAnsw[i].Responses.Count; // calculate all amount of exist answers
            }

            return (int)((procentPlus / allCnt) * 100);
        }

        /// <summary>
        /// Send result to administrator by RabbitMq
        /// </summary>
        /// <returns></returns>
        async Task SendAdminResultAsync(object message)
        {
            await Task.Run(() => _mqService.SendMessage(message));
        }
    }
}
