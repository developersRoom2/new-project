﻿using SharedMongoDBModelLibrary;

namespace CourseTester.Models
{
    public class TestDTO
    {
        public int UserId { get; set; }
        public string CourseId { get; set; }   
        public string LessonId { get; set; }

        public List<Question> answers { get; set; }    
    }
}
