﻿namespace Services.Abstractions
{
    public class IRabbitMqOptions
    {
      public string? QueueNameTo { get; set; }
      public string? QueueNameFrom { get; set; }  
      public string? Host { get; set; }

    }
}
