﻿using Microsoft.AspNetCore.Mvc;
using Homework.DTO;
using Homework.DataAccess;
using Microsoft.AspNetCore.WebUtilities;
using Homework.Services;

namespace Homework.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {
        private readonly IHomeworkRepo _repo;
        private readonly IRabbit _mq;
        public TeacherController(IHomeworkRepo repo, IRabbit mq)
        {
            _repo = repo;
            _mq = mq;
        }

        /// <summary>
        /// Список открытых ДЗ курса
        /// </summary>
        /// <param name="courseId"></param>
        /// <returns></returns>
        [HttpGet]
        public IEnumerable<HomeworkDTO> GetOpen([FromHeader]  string courseId) // смотрим все ДЗ завязанные на Id конкретного курса //здесь string из монги
        {
            return _repo.GetOpen(courseId);
        }

        /// <summary>
        /// Смотрим переписку по ДЗ
        /// </summary>
        /// <param name="homeworkId"></param>
        /// <returns></returns>
        [HttpGet("{homeworkId}")]
        public IEnumerable<MessageDTO> Get(int homeworkId) // смотрим все сообщения на конкретное ДЗ
        {
            return _repo.GetMessages(homeworkId);
        }

        /// <summary>
        /// Принять и поставить оценку
        /// </summary>
        /// <param name="homeworkId"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        [HttpPost]
        public IEnumerable<MessageDTO> AcceptHomework([FromHeader] string homeworkId, [FromHeader]  string value) // Проверка ДЗ (выполненно на сколько-то баллов)
        {

            var r=_repo.Accept(Int32.Parse(homeworkId), Int32.Parse(value));
            r.PercentCorrect = Int32.Parse(value);
            _mq.SendMessage(r, "accept");
            return _repo.GetMessages(Int32.Parse(homeworkId));
        }

        // PUT api/<StudentController>/5
        /// <summary>
        /// Написать сообщение
        /// </summary>
        /// <param name="msg"></param>
        [HttpPut]
        public void AddComment( [FromBody] MessageDTO msg) // добавляем сообщение к конкретному ДЗ
        {
            _repo.AddMesage(msg);

            TeacherMessageDTO teacherMessage = new TeacherMessageDTO() { 
                Text = msg.Text, 
                TeacherId = msg.UserId, 
                ToStudentId = _repo.GetHomework(msg.HomeworkId).StudentId, 
                CreateDate = msg.CreateDate,
                UpDate = msg.UpDate,
                HomeworkId = msg.HomeworkId
            };

            _mq.SendMessage(teacherMessage, "teachermessage"); // signal to Email_Notifier about new message
        }

      
    }
}
