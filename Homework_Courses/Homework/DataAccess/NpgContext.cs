﻿using Homework.Entity;
using Microsoft.EntityFrameworkCore;

namespace Homework.DataAccess
{
    public class NpgContext:DbContext
    {
        public DbSet<Entity.Homework> Homeworks { get; set; }
        public DbSet<Message> Messages { get; set; }
        public NpgContext(DbContextOptions<NpgContext> options) :base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Entity.Homework>(e => {
                e.HasKey("Id");                
                }
            );

            modelBuilder.Entity<Message>(e =>
            {
                e.HasKey("Id");
                e.HasOne(p => p.Homework).WithMany(t => t.Messages).HasForeignKey(p => p.HomeworkId);
            });
        }

    }
}
