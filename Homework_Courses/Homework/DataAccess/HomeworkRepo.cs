﻿using AutoMapper;
using Homework.DTO;
using Homework.Entity;
using Microsoft.EntityFrameworkCore;
using System.Collections;

namespace Homework.DataAccess
{
    public class HomeworkRepo:IHomeworkRepo
    {
        private readonly NpgContext _db;
        private readonly IMapper _mapper;

        public HomeworkRepo(NpgContext db,IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public  IEnumerable<HomeworkDTO> GetOpen(long UserId)
        {
            return _mapper.Map<List<HomeworkDTO>>(_db.Homeworks.Where(p => 
                    p.StudentId == UserId 
                    && p.Completed==false 
                    && p.Deleted==false)
                .OrderByDescending(q=>q.UpDate)
                .ToList());
        }

        public IEnumerable<HomeworkDTO> GetOpen(string CourseId)
        {
            return _mapper.Map<List<HomeworkDTO>>(_db.Homeworks.Where(p =>
                    p.CourseId == CourseId
                    && p.Completed == false
                    && p.Deleted == false)
                .OrderByDescending(q => q.UpDate)
                .ToList());
        }

        public async Task<long> Submit(SubmitDTO work)
        {
            try
            {
                Entity.Homework h = new Entity.Homework
                {
                    CourseId=work.CourseId,
                    LessonId=work.LessonId,
                    CreateDate = DateTime.UtcNow,
                    StudentId=work.StudentId,
                };
                await _db.Homeworks.AddAsync(h);

                Entity.Message m = new Message
                {
                    Homework=h,
                    CreateDate= DateTime.UtcNow,
                    UserId=work.StudentId,
                    Text=work.Text,
                };
                await _db.Messages.AddAsync(m);
                _db.SaveChanges();

                return h.Id;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        public async Task AddMesage(MessageDTO msg)
        {
            try
            {
                Entity.Message m = _mapper.Map<Entity.Message>(msg);
                m.CreateDate = DateTime.UtcNow;
                m.UpDate = DateTime.UtcNow;                
                await _db.Messages.AddAsync(m);
                _db.SaveChanges();
                var t = _db.Homeworks.First(h => h.Id == m.HomeworkId);
                t.UpDate = DateTime.UtcNow;
                _db.Homeworks.Update(t);
                _db.SaveChanges();
            }
            catch { }
        }

        public IEnumerable<MessageDTO> GetMessages(long homeworkId)
        {
            return _mapper.Map<List<MessageDTO>>( _db.Messages.Where(m => m.HomeworkId == homeworkId && m.Deleted == false).OrderByDescending(o => o.CreateDate).ToList());

        }

        public AcceptMessage Accept(int id,int value)
        {
            try
            {
                Entity.Message m = new Message
                {
                    CreateDate = DateTime.UtcNow,
                    UpDate = DateTime.UtcNow,
                    HomeworkId = id,
                    Text = $"Задание зачтено с оценкой {value}",
                    UserId = 0
                };
                _db.Messages.Add(m);                
                var t = _db.Homeworks.First(h => h.Id == id);
                t.UpDate = DateTime.UtcNow;
                t.Completed = true;
                _db.Homeworks.Update(t);
                _db.SaveChanges();
                return  _mapper.Map<AcceptMessage>(  _mapper.Map<HomeworkDTO>(t));
            }
            catch { return null; }
        }

        public HomeworkDTO GetHomework(long homeworkId)
        {
            var entity = _db.Homeworks.Where(m => m.Id == homeworkId && m.Deleted == false).FirstOrDefault();
            return _mapper.Map<HomeworkDTO>(entity);

        }
    }
}
