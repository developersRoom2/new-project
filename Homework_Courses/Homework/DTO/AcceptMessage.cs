﻿namespace Homework.DTO
{
    public class AcceptMessage
    {        
        public long UserId { get; set; }
        public string? CourseId { get; set; }
        public string? LessonId { get; set; }
        public int PercentCorrect { get; set; }
    }
}

