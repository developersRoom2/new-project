﻿using System.ComponentModel.DataAnnotations;

namespace Homework.Entity
{
    public class Homework
    {

        //public int? CompleteValue { get; set; }       // надо ли это здесь хранить?? это хранится в администраторе
        [Key]
        public long Id { get; set; }
        public long StudentId { get; set; }
        public string CourseId { get; set; }
        public string LessonId { get; set; }
        public virtual List<Message> Messages { get; set; }   // Связь с таблицей Messages    
        public DateTime CreateDate { get; set; }
        public DateTime UpDate { get; set; }
        public bool Completed { get; set; }           // надо ли это здесь хранить?? это хранится в администраторе мы только транслируем эти данные в него через Rabbit
                                                        //надо , определять задачи которые сданы но не приняты
        public bool Deleted { get; set; } // Deleted // 1 - deleted, 0 - not deleted
    }
}
