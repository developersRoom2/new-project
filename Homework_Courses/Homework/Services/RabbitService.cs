﻿using RabbitMQ.Client;
using System.Text.Json;
using System.Text;
using System.Configuration;
using Homework.Config;
using Microsoft.Extensions.Options;
using DocumentFormat.OpenXml.Wordprocessing;

namespace Homework.Services
{
    public class RabbitService : IRabbit
    {
        private readonly AppConfig _config;
        public RabbitService(IOptions<AppConfig> config)
        {
            _config = config.Value;
        }

        public void SendMessage(object obj, string severity)
        {
            var message = JsonSerializer.Serialize(obj);
            SendMessage(message, severity);
        }

        public void SendMessage(string message, string severity)
        {
            try
            {
                ConnectionFactory factory;
                //var factory = new ConnectionFactory() { Uri = new Uri(_config.MQconnection) };
                ////factory.Port = 5671;

                if (_config.MQconnection == "localhost")
                {
                    factory = new ConnectionFactory() { HostName = _config.MQconnection }; // for local docker container
                }
                else
                {
                    factory = new ConnectionFactory() { Uri = new Uri(_config.MQconnection) };
                }

                string direct = "direct_homework";

                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    /**Add RabbitMq routing**/
                    channel.ExchangeDeclare(exchange: direct, type: ExchangeType.Direct);

                    //var severity = "tester";

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: direct,
                         routingKey: severity,
                         basicProperties: null,
                         body: body);

                    Console.WriteLine($" [x] Sent '{severity}':'{message}'");


                    //channel.QueueDeclare(queue: _config.MQqueue,
                    //               durable: false,
                    //               exclusive: false,
                    //               autoDelete: false,
                    //               arguments: null);

                    //var body = Encoding.UTF8.GetBytes(message);

                    //channel.BasicPublish(exchange: "",
                    //               routingKey: _config.MQqueue,
                    //               basicProperties: null,
                    //               body: body);
                }
            }
            catch { }
        }
    }
}
