﻿using RabbitMQ.Client.Events;
using RabbitMQ.Client;
using System.Text;
using System.Diagnostics;
using Microsoft.Extensions.Options;
using System.Text.Json;
using CoursesAdministrator.Models;
using CoursesAdministrator.Services;

namespace CoursesAdministrator.RabbitMQ
{
    public class RabbitMqListener : BackgroundService
    {
        private IConnection _connection;
        private IModel _channelTester;
        private IModel _channelHomework;
        private readonly RabbitMQSettings _rabbitMQSettings;
         private readonly IServiceProvider _serviceProvider;

        public RabbitMqListener(IServiceProvider serviceProvider)
        {
            _serviceProvider= serviceProvider;
            using var scope = _serviceProvider.CreateScope();
            var settings = scope.ServiceProvider.GetRequiredService<IOptions<RabbitMQSettings>>();
            
            _rabbitMQSettings = settings.Value;

            ConnectionFactory factory;
            
            if (_rabbitMQSettings.Url == "localhost")
            {
                factory = new ConnectionFactory() { HostName = _rabbitMQSettings.Url }; // for local docker container
            }
            else
            {
                factory = new ConnectionFactory() { Uri = new Uri(_rabbitMQSettings.Url) };
            }

            _connection = factory.CreateConnection();
            
            _channelTester = _connection.CreateModel();
            _channelHomework = _connection.CreateModel();

            _channelTester.QueueDeclare(queue: _rabbitMQSettings.QueueTesterListener, durable: false, exclusive: false, autoDelete: false, arguments: null);
            _channelHomework.ExchangeDeclare(exchange: _rabbitMQSettings.HomeworkExchange, type: ExchangeType.Direct);
        }

        protected  override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            stoppingToken.ThrowIfCancellationRequested();

            var consumerTester = new EventingBasicConsumer(_channelTester);
            consumerTester.Received += (ch, ea) =>
            {
                Console.WriteLine("Receive msg from Tester......");

                var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                
                try
                {
                    using var scope = _serviceProvider.CreateScope();
                    using var progressService = scope.ServiceProvider.GetRequiredService<IProgressService>();

                    var progress = JsonSerializer.Deserialize<UpdateProgressModel>(content);

                    progressService.UpdateProgress(progress).GetAwaiter().GetResult();

                    Debug.WriteLine($"Получено сообщение: {content}");
                }catch(Exception ) { }

                _channelTester.BasicAck(ea.DeliveryTag, false);
            };

            _channelTester.BasicConsume(_rabbitMQSettings.QueueTesterListener, false, consumerTester);

            /*--------------------------------*/
            var queueName_Homework = _channelHomework.QueueDeclare().QueueName;

            _channelHomework.QueueBind(queue: queueName_Homework,
                    exchange: _rabbitMQSettings.HomeworkExchange,
                    routingKey: _rabbitMQSettings.HomeworkTag); // severity <<<<<--------

            Console.WriteLine(" [*] Waiting for messages.");

            var consumerHomework = new EventingBasicConsumer(_channelHomework);
            consumerHomework.Received += (ch, ea) =>
            {
                var body = ea.Body.ToArray();
                var content = Encoding.UTF8.GetString(body);
                var routingKey = ea.RoutingKey;
                Console.WriteLine($" [x] Received '{routingKey}':'{content}'");


                //var content = Encoding.UTF8.GetString(ea.Body.ToArray());
                try
                {
                    if (ea.RoutingKey.Equals(_rabbitMQSettings.HomeworkTag))
                    {
                        using var scope = _serviceProvider.CreateScope();
                        using var progressService = scope.ServiceProvider.GetRequiredService<IProgressService>();

                        var progress = JsonSerializer.Deserialize<UpdateProgressModel>(content);

                        progressService.UpdateHomeworkProgress(progress).GetAwaiter().GetResult();

                        Debug.WriteLine($"Получено сообщение: {content}");
                    }
                }
                catch (Exception ex) 
                {
                    Console.WriteLine("Catch Exception in Rabbit Listener...");
                    Console.WriteLine(ex.ToString());
                }

                //_channelHomework.BasicAck(ea.DeliveryTag, false);
            };

            _channelHomework.BasicConsume(queue: queueName_Homework, 
                autoAck: true, 
                consumer: consumerHomework);

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _channelTester.Close();
            _connection.Close();
            base.Dispose();
        }
    }
}