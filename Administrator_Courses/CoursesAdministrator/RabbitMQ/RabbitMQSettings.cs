﻿namespace CoursesAdministrator.RabbitMQ
{
    public class RabbitMQSettings
    {
        public string Url { get; set; }
        public string QueueTesterListener { get; set; }
        public string HomeworkExchange { get; set; }
        public string HomeworkTag { get; set; }
        public string Direct { get; set; }
    }
}
