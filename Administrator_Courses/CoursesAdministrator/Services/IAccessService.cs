﻿using CoursesAdministrator.Models;
using PostgreSQLModelsLibrary;

namespace CoursesAdministrator.Services
{
    public interface IAccessService
    {
        Task<Access?> GetAccessByCourse(long userId, string courseId);
        Task<IEnumerable<Access?>> GetAccess(long userId);
        Task RequestAccess(AccessModel model);
        Task GiveAccess(AccessModel model);
        Task RevertAccess(AccessModel model);
        Task<IEnumerable<Access?>> GetRequests();
        Task<IEnumerable<long>> GetTeachersByCourse(string courseId);
    }
}
