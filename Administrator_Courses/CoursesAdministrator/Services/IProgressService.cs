﻿using CoursesAdministrator.Models;
using PostgreSQLModelsLibrary;

namespace CoursesAdministrator.Services
{
    public interface IProgressService : IDisposable
    {
        Task AddProgress(ProgressModel model);
        Task UpdateProgress(UpdateProgressModel model);
        Task UpdateHomeworkProgress(UpdateProgressModel model);
        Task<IEnumerable<Progress?>> GetProgress(long userId, string courseId);
        void testc();
    }
}
