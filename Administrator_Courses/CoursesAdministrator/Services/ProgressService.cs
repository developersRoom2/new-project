﻿using CoursesAdministrator.Infrastructure.Repositories.Implementations;
using CoursesAdministrator.Models;
using CoursesAdministrator.RabbitMQ;
using PostgreSQLModelsLibrary;

namespace CoursesAdministrator.Services
{
    public class ProgressService : IProgressService
    {
        private readonly IRepository<Progress> _progressRepository;
        private readonly IRabbitMqService _rabbitMqService;

        public ProgressService(IRepository<Progress> progressRepository, IRabbitMqService rabbitMqService)
        {
            _progressRepository= progressRepository;
            _rabbitMqService= rabbitMqService;
        }

        public void testc()
        {
            _rabbitMqService.SendMessage("jfhjf", "testDone");
        }
        public async Task AddProgress(ProgressModel model)
        {
            await _progressRepository.AddAsync(new Progress()
            {
                CourseId= model.CourseId,
                UserId= model.UserId,
                LessonId= model.LessonId,
                DoneHomework = false,
                IsDone = false
            });
        }

        public async Task UpdateProgress(UpdateProgressModel model)
        {
            var updateProgress = (await _progressRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == model.UserId  && 
                                                                                     a.CourseId.Equals(model.CourseId) && 
                                                                                     a.LessonId.Equals(model.LessonId));
            if (model.PercentCorrect > 50)
            {
                updateProgress.IsDone = true;
                _rabbitMqService.SendMessage(updateProgress, "testdone");
            }
            else
            {
                updateProgress.IsDone = false;
            }

            await _progressRepository.UpdateAsync(updateProgress);
        }

        public async Task UpdateHomeworkProgress(UpdateProgressModel model)
        {
            var updateProgress = (await _progressRepository.GetAllAsync()).FirstOrDefault(a => a.UserId == model.UserId &&
                                                                                     a.CourseId.Equals(model.CourseId) &&
                                                                                     a.LessonId.Equals(model.LessonId));
            if (model.PercentCorrect > 50)
            {
                updateProgress.DoneHomework = true;
                _rabbitMqService.SendMessage(updateProgress, "homeworkdone");
            }
            else
            {
                updateProgress.DoneHomework = false;
            }

            await _progressRepository.UpdateAsync(updateProgress);
        }

        public async Task<IEnumerable<Progress?>> GetProgress(long userId, string courseId)
        {
            return (await _progressRepository.GetAllAsync()).Where(a => a.UserId == userId && a.CourseId.Equals(courseId));
        }

        public void Dispose()
        {
            _progressRepository?.Dispose();
        }
    }
}
