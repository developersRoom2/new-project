﻿using CoursesAdministrator.Models;
using CoursesAdministrator.Services;
using Microsoft.AspNetCore.Mvc;

namespace CoursesAdministrator.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProgressController : ControllerBase
    {
        private readonly IProgressService _progressService;

        public ProgressController(IProgressService progressService)
        {
            _progressService = progressService;
        }

        [HttpGet("{userId}/course/{courseId}")]
        public async Task<IActionResult> GetProgressAsync(long userId, string courseId)
        {
            return Ok(await _progressService.GetProgress(userId, courseId));
        }

        [HttpPost("update")]
        public async Task<IActionResult> UpdateProgressAsync([FromBody] UpdateProgressModel progress)
        {
            await _progressService.UpdateProgress(progress);
            return Ok();
        }

        [HttpPost("add")]
        public async Task<IActionResult> AddProgressAsync([FromBody] ProgressModel progress)
        {
            await _progressService.AddProgress(progress);
            return Ok();
        }
    }
}
