﻿
namespace ProviderCourses.DataAccess
{
    public interface ILessonRepository
    {
       
        List<LessonDTO> Get(string courseId);
        LessonDTO Get(string courseId,string id);
        

    }

}
