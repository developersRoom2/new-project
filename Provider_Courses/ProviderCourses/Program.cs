using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using ProviderCourses.DataAccess;
using System.Data.Common;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddRazorPages();

DBMongoConnect connect = new DBMongoConnect();

builder.Configuration.AddJsonFile("appsettings.json", true);

builder.Services.Configure<CourseStoreDatabaseSettings>(
    builder.Configuration.GetSection("CourseStoreDatabase"));

builder.Services.AddSingleton<ICourseRepository, CourseRepository>();
builder.Services.AddSingleton<ILessonRepository, LessonRepository>();
builder.Services.AddSingleton<IDBConnect>(connect);
builder.Services.AddSingleton<IDBMongoConnectGet>(connect);

builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c => c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First()));


var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run("https://localhost:7078");

