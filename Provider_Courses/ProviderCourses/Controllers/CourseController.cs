﻿using Microsoft.AspNetCore.Mvc;
using ProviderCourses.DataAccess;

namespace ProviderCourses.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class CourseController : ControllerBase
    {

        private readonly ILogger<CourseController> _logger;
        private readonly ICourseRepository _repo;

        public CourseController(ILogger<CourseController> logger, ICourseRepository repo)
        {
            _logger = logger;
            _repo = repo;
        }

        [HttpGet]
        public IEnumerable<CourseDTO> Get() => (IEnumerable<CourseDTO>)_repo.GetAsync().Result;

        [HttpGet("{id}")]
        public CourseDTO Get(string id) => _repo.GetAsync(id).Result;

    }

}
